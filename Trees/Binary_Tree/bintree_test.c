#include"bintree.h"
void main()
{
	NODE root = NULL;
	int item, choice;
	while (1)
	{
		printf(" 1.Insert\n 2.Preorder\n 3.Inorder\n 4.Postorder\n 5.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf("%d", &item);
				root = insert(root, item);
				break;
			case 2:
				preorder(root);
				printf("\n");
				break;
			case 3:
				inorder(root);
				printf("\n");
				break;
			case 4:
				postorder(root);
				printf("\n");
				break;
			default:
				exit(0);
		}
	}
}
