#include"bintree.h"
#include<ctype.h>
#include<math.h>
#define MAX_SIZE 100
NODE stk[MAX_SIZE];
NODE create_exp_tree(char postfix[])
{
        int i, j = 0;
        NODE temp;
        for (i = 0; i < strlen(postfix); i++)
        {
                temp = get_node();
                temp->info = postfix[i];
                if (isalnum(postfix[i]))
                {
                        temp->left = temp->right = NULL;
                        stk[j++] = temp;
                }
                else
                {
                       temp->right = stk[--j];
                       temp->left = stk[--j];
                       stk[j++] = temp;
                }
        }
	if (j == 1)
	        return stk[0];
	printf("Invalid Expression\n");
	return NULL;
}
float eval(NODE root)
{
	float num;
	if (root == NULL)
		return 0.0;
	switch (root->info)
	{
		case '+':
			return eval(root->left) + eval(root->right);
		case '-':
			return eval(root->left) - eval(root->right);
		case '*':
			return eval(root->left) * eval(root->right);
		case '/':
			return eval(root->left) / eval(root->right);
		case '^':
		case '$':
			return pow(eval(root->left), eval(root->right));
		default:
			if (isalpha(root->info))
			{
				printf("Enter value of %c: ", root->info);
				scanf("%f", &num);
				return num;
			}
			else
				return (root->info - '0');
	}
}
void main()
{
        char postfix[30];
        NODE root;
        printf("Enter the postfix expression\n");
        scanf("%s", postfix);
        root = create_exp_tree(postfix);
	printf("Inorder: ");
        inorder(root);
	printf("\n");
	printf("Postorder: ");
	postorder(root);
	printf("\n");
	printf("Preorder");
	preorder(root);
	printf("\n");
	printf("\n%f\n", eval(root));
}
