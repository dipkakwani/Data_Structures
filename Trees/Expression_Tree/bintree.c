#include"bintree.h"
NODE get_node()
{
	NODE temp;
	temp = (NODE) malloc(sizeof(struct Node));
	if (temp == NULL)
		printf("Memory can not be allocated\n");
	return temp;
}
void preorder(NODE root)
{
	if (root != NULL)
	{
		printf("%c\t", root->info);
		preorder(root->left);
		preorder(root->right);
	}
}
void inorder(NODE root)
{
	if (root != NULL)
	{
		inorder(root->left);
		printf("%c\t", root->info);
		inorder(root->right);
	}
}
void postorder(NODE root)
{
	if (root != NULL)
	{
		postorder(root->left);
		postorder(root->right);
		printf("%c\t", root->info);

	}
}
NODE insert(NODE root, char item)
{
	int i;
	NODE temp, cur, prev;
	char path[30];
	temp = get_node();
	temp->info = item;
	temp->left = temp->right = NULL;
	if (root == NULL)
		return temp;
	printf("Enter the path\n");
	scanf("%s", path);
	for (i = 0; i < strlen(path); i++)
		toupper(path[i]);
	for (cur = root, prev = NULL, i = 0; i < strlen(path) && cur != NULL; i++)
	{
		prev = cur;
		if (path[i] == 'L')
			cur = cur->left;
		else
			cur = cur->right;
	}
	if (cur != NULL || i != strlen(path))
	{
		printf("Cannot insert\n");
		free(temp);
		return root;
	}
	if (path[i - 1] == 'L')
		prev->left = temp;
	else
		prev->right = temp;
	return root;
}
