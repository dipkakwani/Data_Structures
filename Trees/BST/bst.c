#include"bst.h"
Node get_node()
{
	Node temp;
        temp = (Node)malloc(sizeof(struct node));
        if (temp == NULL)
		printf("Memory can not be allocated\n");
	return temp;
}
void free_node(Node temp)
{
	free(temp);
}
Node insert(Node root, int item)
{
	if (root == NULL)
	{
        	Node temp;
        	temp = get_node();
        	temp->info = item;
	        temp->right = temp->left = NULL;
        	return temp;
	}
        if (item > root->info)
        	root->right = insert(root->right, item);
        else
        	root->left = insert(root->left, item);
	return root;
}
Node delete(Node root, int item)	//Deletion by merging
{
	Node cur, temp, prev;
	if (root == NULL)
	{
		printf("Empty BST\n");
		return root;
	}
	cur = root;
	prev = NULL;
	while (cur != NULL && cur->info != item)	//Searching the key
	{
		prev = cur;
		if (cur->info > item)
			cur = cur->left;
		else
			cur = cur->right;
	}
	if (cur == NULL)
	{
		printf("Key not found\n");
		return root;
	}
	printf("Item deleted %d\n", cur->info);
	if (cur->right == NULL || cur->left == NULL)		//Either Leaf Node or a Node with only one child
	{
		if (cur->right == NULL)
			temp = cur->left;
		else
			temp = cur->right;
		if (prev == NULL)	//Root Node
			root = temp;
		else
		{
			if (prev->right == cur)
				prev->right = temp;
			else
				prev->left = temp;
		}
		free_node(cur);
	}
	//Two children
	//Find the successor of current node in the right subtree
	else
	{
		temp = cur->right;
		while (temp->left != NULL)
			temp = temp->left;
		temp->left = cur->left;
		temp = cur;
		if (prev == NULL)	//Root Node
			root = cur->right;
		else
		{
			if (prev->right == cur)
				prev->right = cur->right;
			else
				prev->left = cur->right;
		}
		free_node(temp);
	}
	return root;
}
void prefix(Node root)         //Preorder Traversal
{
	if (root == NULL)
        	return;
        printf("%d\t", root->info);
        prefix(root->left);
        prefix(root->right);
}
void postfix(Node root)         //Preorder Traversal
{
	if (root == NULL)
        	return;
        postfix(root->left);
        postfix(root->right);
        printf("%d\t", root->info);
}
void infix(Node root)         //Preorder Traversal
{
	if (root == NULL)
        	return;
        infix(root->left);
        printf("%d\t", root->info);
        infix(root->right);
}
