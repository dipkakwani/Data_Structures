#ifndef BST_H
#define BST
#include<stdio.h>
#include<stdlib.h>
struct node
{
    int info;
    struct node* left;
    struct node* right;
};
typedef struct node* Node;
Node get_node();
inline void free_node(Node temp);
Node insert(Node root, int item);
Node delete(Node root, int item);
#endif // BST_H
