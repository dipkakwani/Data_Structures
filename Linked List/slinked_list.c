#include"sllist.h"
void main()
{
	NODE front = NULL;
	int choice, item, pos;
	while(1)
	{
		printf("Enter\n1.Insert front\n2.Insert rear\n3.Delete front\n4.Delete rear\n5.Display\n6.Search\n");
		printf("7.Insert at position\n8.Delete from a position\n8.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter item to be inserted\n");
				scanf("%d", &item);
				front = insert_front(front, item);
				break;
			case 2: 
				printf("Enter item to be inserted\n");
				scanf("%d", &item);
				front = insert_rear(front, item);
				break;
			case 3:
				front = delete_front(front);
				break;
			case 4:
				front = delete_rear(front);
				break;
			case 5:
				display(front);
				break;
			case 6:
				printf("Enter the item to search\n");
				scanf("%d", &item);
				search(front, item);
				break;
			case 7:
				printf("Enter the item to insert and position\n");
				scanf("%d %d", &item, &pos);
				front = insert_pos(front, item, pos);
				break;
			case 8:
				printf("Enter the position\n");
				scanf("%d", &pos);
				front = delete_pos(front, pos);
				break;
			default:
				exit(0);
		}
	}
}
