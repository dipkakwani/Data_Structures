#include"linked_list.h"
NODE merge(NODE front1, NODE front2)
{
	NODE cur1, cur2, result = NULL;
	cur1 = front1;
	cur2 = front2;
	while (cur1 != NULL && cur2 != NULL)
	{
		if (cur1->info > cur2->info)
		{
			result = insert_rear(result, cur2->info);
			cur2 = cur2->next;
		}
		else
		{
			result = insert_rear(result, cur1->info);
			cur1 = cur1->next;
		}
		
	}
	for (; cur1 != NULL; cur1 = cur1->next)
		result = insert_rear(result, cur1->info);
	for (; cur2 != NULL; cur2 = cur2->next)
		result = insert_rear(result, cur2->info);
	return result;
}
NODE insert(NODE front, int item)
{
	NODE cur, prev, temp;
	temp = get_node();
	temp->info = item;
	temp->next = NULL;
	if (isEmpty(front))
		return temp;
	if (temp->info < front->info)
	{
		temp->next = front;
		return temp;
	}
	for (cur = front, prev = NULL; cur != NULL && cur->info < temp->info; prev = cur, cur = cur->next);
	prev->next = temp;
	temp->next = cur;
	return front;
}
void main()
{
	NODE front1 = NULL, front2 = NULL, res = NULL;
	int item, n1, n2;
	printf("Enter size of the two linked lists\n");
	scanf("%d %d" , &n1, &n2);
	printf("Enter elements of 1st linked list\n");
	while (n1--)
	{
		printf("Enter item\n");
		scanf("%d", &item);
		front1 = insert(front1, item);
	}
	printf("Enter elements of 2nd linked list\n");
	while (n2--)
	{
		printf("Enter item\n");
		scanf("%d", &item);
		front2 = insert(front2, item);
	}
	res = merge(front1, front2);
	printf("Merged List\n");
	display(res);
}
