#include"linked_list.h"
NODE concat(NODE front1, NODE front2)
{
	NODE cur;
	if (isEmpty(front1))
		return front2;
	if (isEmpty(front2))
		return front1;
	for (cur = front1; cur->next != NULL; cur = cur->next);
	cur->next = front2;
	return front1;
}
void main()
{
	NODE front1 = NULL, front2 = NULL;
	int item, n1, n2;
	printf("Enter size of the two linked lists\n");
	scanf("%d %d", &n1, &n2);
	printf("Enter elements of 1st linked list\n");
	while (n1--)
	{
		printf("Enter item\n");
		scanf("%d", &item);
		front1 = insert_rear(front1, item);
	}
	printf("Enter elements of 2nd linked list\n");
	while (n2--)
	{
		printf("Enter item\n");
		scanf("%d", &item);
		front2 = insert_rear(front2, item);
	}
	front1 = concat(front1, front2);
	printf("Concatenated Linked list\n");
	display(front1);

}
