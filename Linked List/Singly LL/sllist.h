#ifndef SLLIST_H
#define SLLIST_H
#include<stdio.h>
#include<stdlib.h>
struct node{
	int info;
	struct node *next;
};
typedef struct node *NODE;
int isEmpty(NODE front);
NODE get_node();
void free_node(NODE temp);
NODE insert_front(NODE front, int item);
NODE insert_rear(NODE front, int item);
NODE insert_pos(NODE front, int item, int pos);
NODE delete_front(NODE front);
NODE delete_rear(NODE front);
NODE delete_pos(NODE front, int pos);
void display(NODE front);
void search(NODE front, int item);
#endif
