#include"cllist.h"
NODE get_node()
{
	NODE temp;
	temp = (NODE)malloc(sizeof(struct Node));
	if (temp == NULL)
		printf("Memory can not be allocated\n");
	return temp;
}
int isEmpty(NODE rear)
{
	return (rear== NULL);
}
void free_node(NODE temp)
{
	free(temp);
}
NODE insert_front(NODE rear, int item)
{
	NODE temp;
	temp = get_node();
	if (temp != NULL)
	{
		temp->info = item;
		if (isEmpty(rear))
			rear = temp;
		else
			temp->next = rear->next;
		rear->next = temp;
		return rear;
	}
	return rear;
}
NODE insert_rear(NODE rear, int item)
{
	NODE temp;
	temp = get_node();
	if (temp != NULL)
	{
		temp->info = item;
		if (isEmpty(rear))
			rear = temp;
		else
			temp->next = rear->next;
		rear->next = temp;
		return temp;
	}
	return rear;
}
NODE delete_front(NODE rear)
{
	NODE temp;
	if(isEmpty(rear))
	{
		printf("Empty List\n");
		return rear;
	}
	temp = rear->next;
	printf("Item deleted: %d\n", temp->info);
	if (rear->next == rear)		//Only one element in the linked list
	{
		free_node(temp);
		return NULL;
	}
	rear->next = temp->next;
	free_node(temp);
	return rear;
}
NODE delete_rear(NODE rear)
{
	NODE temp;
	if(isEmpty(rear))
	{
		printf("Empty List\n");
		return rear;
	}
	temp = rear;
	printf("Item deleted: %d\n", temp->info);
	while (rear->next != temp)
		rear= rear->next;
	if (rear->next == rear)		//Only one element in the linked list
	{
		free_node(temp);
		return NULL;
	}
	rear->next = temp->next;
	free_node(temp);
	return rear;
}
void display(NODE rear)
{
	NODE cur;
	if (isEmpty(rear))
	{
		printf("Empty list\n");
		return;
	}
	cur = rear->next;
	do
	{
		printf("%d\n", cur->info);
		cur = cur->next;
	}while(cur != rear->next);
}
