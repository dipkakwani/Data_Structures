#include"cllist.h"
NODE concat(NODE rear1, NODE rear2)
{
	NODE temp;
	if (rear1 == NULL)
		return rear2;
	if (rear2 == NULL)
		return rear1;
	temp = rear2->next;
	rear2->next = rear1->next;
	rear1->next = temp;
	return rear2;
}
void main()
{
	NODE rear1 = NULL, result = NULL, rear2 = NULL;
	int item, choice;
	while (1)
	{
		printf("Enter\n 1.Insert in first list\n 2.Insert in second list\n 3.Display first list\n 4.Display second list\n");
		printf(" 5.Concatenate both lists\n 6.Exit\n");
		scanf("%d", &choice);
		switch (choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf("%d", &item);
				rear1 = insert_rear(rear1, item);
				break;
			case 2:
				printf("Enter the item\n");
				scanf("%d", &item);
				rear2 = insert_rear(rear2, item);
				break;
			case 3:
				display(rear1);
				break;
			case 4:
				display(rear2);
				break;
			case 5:
				result = concat(rear1, rear2);
				printf("Result list\n");
				display(result);
				break;
			default:
				exit(0);
		}

	}
}
