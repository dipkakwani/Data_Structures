#ifndef CLLIST_H
#define CLLIST_H
#include<stdio.h>
#include<stdlib.h>
struct Node
{
	int info;
	struct Node* next;
};
typedef struct Node* NODE;
NODE get_node();
int isEmpty(NODE rear);
void free_node(NODE temp);
NODE insert_rear(NODE rear, int item);
NODE insert_front(NODE rear, int item);
NODE delete_front(NODE rear);
NODE delete_rear(NODE rear);
void display(NODE front);
#endif
