#include"cllist.h"
void main()
{
	int item, choice;
	NODE rear = NULL;
	while (1)
	{
		printf("Enter\n 1.Enqueue\n 2.Dequeue\n 3.Display\n 4.Exit\n");
		scanf("%d", &choice);
		switch (choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf("%d", &item);
				rear = insert_rear(rear, item);
				break;
			case 2:
				rear = delete_front(rear);
				break;
			case 3:
				display(rear);
				break;
			default:
				exit(0);
		}
	}
}
