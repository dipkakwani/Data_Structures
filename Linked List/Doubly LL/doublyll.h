#ifndef DLLIST_H
#define DLLIST_H
#include<stdio.h>
#include<stdlib.h>
struct node
{
	int info;
	struct node* next;
	struct node* prev;
};
typedef struct node* NODE;
void free_node(NODE temp);
NODE get_node();
int isEmpty(NODE head);
NODE insert_front(NODE head, int item);
NODE insert_rear(NODE head, int item);
NODE insert_pos(NODE head, int item, int pos);
NODE delete_front(NODE head);
NODE delete_rear(NODE head);
void display(NODE head);
#endif
