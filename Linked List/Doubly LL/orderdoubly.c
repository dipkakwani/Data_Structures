#include"doublyll.h"
NODE order_insert(NODE head, int item)
{
	NODE temp, cur, prev;
	temp = get_node();
	temp->info = item;
	for (cur = head->next, prev = head; cur != NULL && item > cur->info; prev = cur, cur = cur->next);
	temp->next = cur;
	temp->prev = prev;
	prev->next = temp;
	if (cur != NULL)
		cur->prev = temp;
	return head;
}
void main()
{
	NODE head;
	int item, choice;
	head = get_node();
	head->info = 0;
	head->prev = NULL;
	head->next = NULL;
	while (1)
	{
		printf(" 1.Insert\n 2.Delete front\n 3.Delete Rear\n 4. Display\n 5.Exit\n");
		scanf("%d", &choice);
		switch (choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf("%d", &item);
				head = order_insert(head, item);
				break;
			case 2:
				head = delete_front(head);
				break;
			case 3:
				head = delete_rear(head);
				break;
			case 4:
				display(head);
				break;
			default:
				exit(0);
		}

	}
}
