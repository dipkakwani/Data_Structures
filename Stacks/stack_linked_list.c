#include"stackll.h"
void main()
{
	NODE front = NULL;
	int choice, item;
	while(1)
	{
		printf("Enter\n1.Push\n2.Pop\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter item\n");
				scanf("%d", &item);
				front = insert_front(front, item);
				break;
			case 2:
				front = delete_front(front);
				break;
			case 3:
				display(front);
				break;
			default:
				exit(0);
		}
	}
}
