#include"stack.h"
int isFull(stk *s)
{
	return ((s->top) == SIZE - 1);
}
int isEmpty(stk *s)
{
	return ((s->top) == -1);
}
void push(stk *s, char item)
{
	if(isFull(s))
	{
		printf("Stack Overflow\n");
		return;
	}
	s->arr[++(s->top)] = item;
}
void display(stk *s)
{
	int i;
	if(isEmpty(s))
	{
		printf("Empty Stack\n");
		return;
	}
	printf("%d", s->top);
	for(i = 0; i <= s->top; i++)
		printf("%c\n", s->arr[i]);
}

int pop(stk *s)
{
	char del;
	if(isEmpty(s))
	{
		printf("Stack Underflow\n");
		return 0;
	}
	del = s->arr[s->top];
	--(s->top);
	return del;
}
