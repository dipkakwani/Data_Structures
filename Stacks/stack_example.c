#include "stack.h"
void main()
{
	stk s;
	char item;
	s.top = -1;
	int choice;
	while(1)
	{
		printf("\nEnter\n1.Push\n2.Pop\n3.Display\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf(" %c", &item);
				push(&s, item);
				break;
			case 2:
				pop(&s);
				break;
			case 3:
				display(&s);
				break;
			default:
				exit(0);
		}
	}
}

