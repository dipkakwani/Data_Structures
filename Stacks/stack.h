#ifndef STACK_H
#include<stdio.h>
#include<stdlib.h>
#define STACK_H
#define SIZE 100
struct stack{
	int top;
	char arr[SIZE];
};
typedef struct stack stk;
int isFull(stk *s);
int isEmpty(stk *s);
void push(stk *s, char item);
int pop(stk *s);
void display(stk *s);
#endif
