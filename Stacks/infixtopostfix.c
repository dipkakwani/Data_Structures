#include<stdio.h>
#include<ctype.h>
#include<string.h>
#define SIZE 50

//Push to Stack
void push(char s[], int *top, char item)
{
	s[++(*top)] = item;
}

//Pop from Stack
char pop(char s[], int *top)
{
	char del;
	del = s[*top];
	(*top)--;
	return del;
}
//Is Stack Empty
int empty(int top)
{
	return (top == -1);
}

/*Checks Precedence of two operators
Returns True if precedence of op1 is greater than op2, else false
*/
int prec(char op1, char op2)
{
	switch(op1)
	{
		case'+':
		case '-':
			if(op2 == ')')
				return 1;
			return 0;
		case'*':
		case'/':
			if(op2 == ')' || op2 == '+' || op2 == '-')
				return 1;
			return 0;
		case'$':
		case'^':
			if(op2 == '(' || op2 == '$' || op2 == '^')
				return 0;
			return 1;
		case'(':
			return 0;		//'(' should not go to postfix
	}
}
//Function to convert infix to postfix
void intopost(char infix[], char postfix[])
{
	int i, j = 0, top = -1;
	char sym, s[30];
	for(i = 0; i < strlen(infix); i++)
	{
		sym = infix[i];
		if(isalpha(sym) || isdigit(sym))
			postfix[j++] = sym;
		else
		{
			while(!empty(top) && prec(s[top], sym))
				postfix[j++] = pop(s, &top);
			if(sym == ')')
				pop(s, &top);		//pop '('
			else
				push(s, &top, sym);
		}
	}
	while(top != -1)
		postfix[j++] = pop(s, &top);
	postfix[j] = '\0';
}

void main()
{
	char infix[SIZE], postfix[SIZE];
	printf("Enter infix string\n");
	scanf("%s", infix);
	intopost(infix, postfix);
	printf("Postfix:\t%s\n", postfix);
}
