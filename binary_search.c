#include<stdio.h>
int binsearch(int a[], int key, int low, int high)
{
	int mid;
	if(low > high)
	{
		printf("Key not found\n");
		return -1;
	}
	mid = (low + high) / 2;
	if(a[mid] == key)
		return mid + 1;
	else if(a[mid] > key)
		return binsearch(a, key, low, mid - 1);
	else
		return binsearch(a, key, mid + 1, high);	
}
void main()
{
	int a[10], n, key, i, res;
	printf("Enter number of elements\n");
	scanf("%d", &n);
	printf("Enter the array\n");
	for(i  = 0; i < n; i++)
		scanf("%d", &a[i]);
	printf("Enter the key\n");
	scanf("%d", &key);
	res = binsearch(a,key, 0, n - 1);
	if(res != -1) 
		printf("Key found at %d\n", res);
}
