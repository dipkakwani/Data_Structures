#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct student
{
	int usn;
	char name[30];
	float m1, m2, m3;
};
typedef struct student STD;
FILE* file_open(char file_name[], char mode[])
{
	FILE* fp;
	fp = fopen(file_name, mode);
	if (fp == NULL)
		printf("File can not be openend\n");
	return fp;
}
void append_record(FILE* fp)
{
	STD s;
	printf("Enter USN: ");
	scanf("%d", &s.usn);
	printf("Enter the name: ");
	scanf("%s", s.name);
	printf("Enter the maks: ");
	scanf("%f %f %f", &s.m1, &s.m2, &s.m3);
	fprintf(fp, "%d %s %f %f %f", s.usn, s.name, s.m1, s.m2, s.m3);
}
int search_record(FILE* fp, int key)
{
	STD s;
	while (!feof(fp))
	{
		fscanf(fp, "%d %s %f %f %f", &s.usn, s.name, &s.m1, &s.m2, &s.m3);
		if (s.usn == key)
			return 1;
	}
	return 0;
}
void display(FILE* fp)
{
	STD s;
	while (!feof(fp))
	{
		fscanf(fp, "%d %s %f %f %f", &s.usn, s.name, &s.m1, &s.m2, &s.m3);
		printf("USN: %d\n", s.usn);	
		printf("Name: %s\n", s.name);
		printf("Marks: %f %f %f\n", s.m1, s.m2, s.m3);
	}
}
void main()
{
	FILE* fp;
	char file_name[30];
	int key, choice;
	printf("Enter the file name\n");
	scanf("%s", file_name);
	while (1)
	{
		printf(" 1.Insert\n 2.Search\n 3.Display\n 4.Exit\n");
		scanf(" %d", &choice);
		switch(choice)
		{
			case 1:
				fp = file_open(file_name, "a+");
				if (fp == NULL)
					break;
				append_record(fp);
				fclose(fp);
				break;
			case 2:
				fp = file_open(file_name, "r");
				if (fp == NULL)
					break;
				printf("Enter the key\n");
				scanf("%d", &key);
				if (search_record(fp, key))
					printf("Found the key\n");
				else
					printf("Key not found\n");
				break;
			case 3:
				fp = file_open(file_name, "r");
				if (fp == NULL)
					break;
				display(fp);
				break;
			default:
				exit(0);
		}
	}
}
