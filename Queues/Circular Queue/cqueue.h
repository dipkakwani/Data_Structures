#ifndef CQUEUE_H
#include<stdio.h>
#define CEQUEUE_H
#define SIZE 50
struct queue{
	int front, rear;
	char arr[SIZE];
};
typedef struct queue Q;
int isEmpty(int count);
int isFull(int count);
void enqueue(Q *q, char item, int *count);
char dequeue(Q *q, int *count);
void display(Q *q, int count);
#endif
