#include"cqueue.h"
void main()
{
	int choice, count = 0;
	char item;
	Q q;
	q.front =0;
	q.rear = -1;
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf(" %c", &item);
				enqueue(&q, item, &count);
				break;
			case 2:
				printf("Item deleted: %c\n", dequeue(&q, &count));
				break;
			case 3:
				display(&q, count);
				break;
			default:
				exit(0);
		}
	}
}
