#include"cqueue.h"
int isEmpty(int count)
{
	return (count == 0);
}
int isFull(int count)
{
	return (count == SIZE - 1);
}
void enqueue(Q *q, char item, int *count)
{
	if(isFull(*count))
	{
		printf("Queue Overflow\n");
		return;
	}
	q->rear = (q->rear + 1) % SIZE;
	q->arr[q->rear] = item;
	(*count)++;
}
void display(Q *q, int count)
{
	int i, j;
	if(isEmpty(count))
	{
		printf("Empty Queue\n");
		return;
	}
	for(i = q->front, j = 0; j < count; j++)
	{
		printf("%c\n", q->arr[i]);
		i = (i + 1) % SIZE;
	}
}
char dequeue(Q *q, int *count)
{
	char del;
	if(isEmpty(*count))
	{
		printf("Queue Underflow\n");
		return '0';
	}
	del = q->arr[q->front];
	q->front = (q->front + 1) % SIZE;
	(*count)--;
	return del;
	
}
