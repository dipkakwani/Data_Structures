#include"lqueue.h"
void insert_priority(Q *q, int item)
{
	int i;
	if(isFull(q))
	{
		printf("Queue Overflow");
		return;
	}
	i = q->rear;
	while(i >= 0 && item < q->arr[i])
	{
		q->arr[i + 1] = q->arr[i];
		i--;
	}
	q->arr[i + 1] = item;
	(q->rear)++;
}
void main()
{
	int choice;
	int item;
	Q q;
	q.front =0;
	q.rear = -1;
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf(" %d", &item);
				insert_priority(&q, item);
				break;
			case 2:
				printf("Deleted item : %d\n", dequeue(&q));
				break;
			case 3:
				display(&q);
				break;
			default:
				exit(0);
		}
	}
}
