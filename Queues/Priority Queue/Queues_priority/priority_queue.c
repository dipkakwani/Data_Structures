#include"lqueue.h"
void main()
{
	int choice, priority;
	int item, i;
	Q q[3];
	for(i = 0; i < 3; i++)
	{
		q[i].front =0;
		q[i].rear = -1;
	}
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item and the priority\n");
				scanf(" %d %d", &item, &priority);
				enqueue(&q[priority - 1], item);	
				break;
			case 2:
				i = 0;
				while(i < 3 && isEmpty(&q[i]))
					i++;
				if(i == 4)
					printf("Queue Underflow\n");
				else
					printf("Item deleted: %d\n", dequeue(&q[i]));
				break;
			case 3:
				for(i = 0; i < 3; i++)
					display(&q[i]);
				break;
			default:
				exit(0);
		}
	}
}
