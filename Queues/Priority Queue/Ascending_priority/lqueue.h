#ifndef LQUEUE_H
#include<stdio.h>
#define LEQUEUE_H
#define SIZE 50
struct queue{
	int front, rear;
	int arr[SIZE];
};
typedef struct queue Q;
int isEmpty(Q *q);
int isFull(Q *q);
void enqueue(Q *q, int item);
int dequeue(Q *q);
void display(Q *q);
#endif
