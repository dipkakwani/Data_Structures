#ifndef LQUEUE_H
#include<stdio.h>
#define LEQUEUE_H
#define SIZE 50
struct queue{
	int front, rear;
	char arr[SIZE];
};
typedef struct queue Q;
int isEmpty(Q *q);
int isFull(Q *q);
void enqueue(Q *q, char item);
char dequeue(Q *q);
void display(Q *q);
#endif
