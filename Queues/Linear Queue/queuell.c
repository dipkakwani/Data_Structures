#include"queuell.h"
int isEmpty(NODE front)
{
	return (front == NULL);
}
NODE get_node()
{
    NODE temp;
    temp = (NODE)malloc(sizeof(struct Node));
    if(temp == NULL)
        printf("Not enough memory!\n");
    return temp;
}
void free_node(NODE temp)
{
	free(temp);
}
NODE insert_rear(NODE front, int item)
{
	NODE temp, cur;
	temp = get_node();
	if (temp != NULL)
	{
		temp->info = item;
		temp->next = NULL;
		if (isEmpty(front))
			return temp;
		for (cur = front; cur->next = NULL; cur = cur->next);
		cur->next = temp;
		return front;
	}
	return NULL;
}
NODE delete_front(NODE front)
{
	NODE temp;
	if (isEmpty(front))
	{
		printf("Stack Underflow\n");
		return NULL;
	}
	temp = front;
	printf("Deleted Item:%d\n", front->info);
	front = front->next;
	free(temp);
	return front;
}
void display(NODE front)
{
	NODE cur;
	if(isEmpty(front))
	{
		printf("Empty stack\n");
		return;
	}
	for (cur = front; cur != NULL; cur = cur->next)
		printf("%d\n", cur->info); 
}
