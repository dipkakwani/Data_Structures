#include<stdio.h>
#define SIZE 50
int isEmpty(int front, int rear)
{
	return (front > rear);
}
int isFull(int rear)
{
	return (rear == SIZE - 1);
}
void enqueue(char a[], int *rear, char item)
{
	if(isFull(*rear))
	{
		printf("Queue Overflow\n");
		return;
	} 
	a[++(*rear)] = item;
}
char dequeue(char a[], int *front, int *rear)
{
	char del;
	if(isEmpty(*front, *rear))
	{
		printf("Queue Underflow\n");
		return '0';
	}
	del = a[*front];
	(*front)++;
	if(*front > *rear)		//Deleted the only element in the queue
	{
		*front = 0;
		*rear = -1;
	}
	return del;
}
void display(char a[], int front, int rear)
{
	int i;
	if(isEmpty(front, rear))
	{
		printf("Empty Queue\n");
		return;
	}
	for(i = front; i <= rear; i++)
		printf("%c\n", a[i]);
}
void main()
{
	char a[SIZE], item, del;
	int front = 0, rear = -1, choice;
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf(" %c", &item);
				enqueue(a, &rear, item);
				break;
			case 2:
				del = dequeue(a, &front, &rear);
				if(del != '0')
					printf("Element deleted: %c\n", del);
				break;
			case 3:
				display(a, front, rear);
				break;
			default:
				exit(0);
		}
	}
}

